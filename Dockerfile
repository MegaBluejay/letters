# syntax = docker/dockerfile:1

ARG APP_NAME=letters
ARG APP_PATH=/opt/$APP_NAME
ARG PYTHON_VERSION=3.11.3
ARG POETRY_VERSION=1.5.1

FROM python:$PYTHON_VERSION as base
ARG APP_PATH
ARG POETRY_VERSION

ENV \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1
ENV \
    POETRY_VERSION=$POETRY_VERSION \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_NO_INTERACTION=1

RUN curl -sSL https://install.python-poetry.org | python3 -
ENV PATH="$POETRY_HOME/bin:$PATH"

WORKDIR $APP_PATH

COPY ./pyproject.toml .
COPY ./poetry.lock .
RUN --mount=type=cache,target=/root/.cache/pypoetry/cache \
    poetry install --no-root --no-directory --only main


FROM base as dev
ARG APP_PATH

WORKDIR $APP_PATH
COPY . .
RUN --mount=type=cache,target=/root/.cache/pypoetry/cache \
    poetry install --with dev

FROM base as build
ARG APP_PATH

WORKDIR $APP_PATH
COPY . .
RUN poetry install --only main
RUN ./manage.py collectstatic --noinput
RUN poetry build --format wheel
RUN poetry export --only main --without-hashes -f constraints.txt -o constraints.txt

FROM python:$PYTHON_VERSION as prod
ARG APP_PATH

ENV \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1

ENV \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100

WORKDIR $APP_PATH
COPY --from=build $APP_PATH/dist/*.whl .
COPY --from=build $APP_PATH/constraints.txt .
COPY --from=build $APP_PATH/static ./static
RUN --mount=type=cache,target=/root/.cache/pip \
     pip install ./*.whl -c constraints.txt
