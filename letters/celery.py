import os
from logging.config import dictConfig

import structlog
from celery import Celery
from celery.signals import setup_logging
from django.conf import settings
from django_structlog.celery.steps import DjangoStructLogInitStep

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "letters.settings")

app = Celery("letters")

app.config_from_object(settings, namespace="CELERY")

app.steps["worker"].add(DjangoStructLogInitStep)


# noinspection PyUnusedLocal
@setup_logging.connect
def on_setup_logging(loglevel, logfile, format, colorize, **_):
    dictConfig(settings.LOGGING)
    structlog.configure(**settings.STRUCTLOG_SETTINGS)


app.autodiscover_tasks()
