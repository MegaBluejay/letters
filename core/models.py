from django.db import models
from django.utils.translation import gettext_lazy as _
from timezone_field import TimeZoneField


class Tag(models.Model):
    name = models.CharField(max_length=255, primary_key=True)


class Mailing(models.Model):
    start_dtm = models.DateTimeField()
    end_dtm = models.DateTimeField()
    text = models.TextField()
    op_code = models.CharField(max_length=3)
    tags = models.ManyToManyField(Tag, related_name="mailings")
    handled = models.BooleanField(default=False)
    interval_start = models.TimeField(null=True)
    interval_end = models.TimeField(null=True)


class Client(models.Model):
    phone = models.CharField(max_length=11)
    tags = models.ManyToManyField(Tag, related_name="clients")
    timezone = TimeZoneField()

    @property
    def op_code(self) -> str:
        return self.phone[1:4]


class Message(models.Model):
    class Status(models.TextChoices):
        PENDING = "pending", _("Pending")
        SENT = "sent", _("Sent")
        FAILED = "failed", _("Failed")
        CANCELLED = "cancelled", _("Cancelled")

    class Failure(models.TextChoices):
        API_FAILURE = "api_failure", _("API Failure")
        INTERVAL = "interval", _("Interval")

    sent_dtm = models.DateTimeField(null=True)
    status = models.CharField(max_length=max(map(len, Status.values)), choices=Status.choices)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name="messages")
    client = models.ForeignKey(Client, on_delete=models.CASCADE, related_name="messages")
    failure = models.CharField(max_length=max(map(len, Failure.values)), choices=Failure.choices, null=True)
