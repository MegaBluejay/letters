import structlog
from drf_spectacular.utils import extend_schema
from rest_framework import viewsets
from rest_framework.decorators import action

from .models import Client, Mailing
from .serializers import ClientSerializer, MailingSerializer, MailingStatSerializer
from .stats import with_stats


# noinspection PyUnresolvedReferences
class StructLogViewSet:
    contextvar = "id"
    id_field = "id"

    def get_object(self):
        obj = super().get_object()
        structlog.contextvars.bind_contextvars(**{self.contextvar: getattr(obj, self.id_field)})
        return obj

    def perform_create(self, serializer):
        res = super().perform_create(serializer)
        structlog.contextvars.bind_contextvars(**{self.contextvar: getattr(serializer.instance, self.id_field)})
        return res


@extend_schema(tags=["clients"])
class ClientViewSet(StructLogViewSet, viewsets.ModelViewSet):
    contextvar = "client_id"

    queryset = Client.objects.all()
    serializer_class = ClientSerializer


@extend_schema(tags=["mailing"])
class MailingViewSet(StructLogViewSet, viewsets.ModelViewSet):
    contextvar = "mailing_id"

    queryset = Mailing.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset()
        if self.action in ["all_stats", "single_stats"]:
            queryset = with_stats(queryset)
        return queryset

    def get_serializer_class(self):
        if self.action in ["all_stats", "single_stats"]:
            return MailingStatSerializer
        return MailingSerializer

    @extend_schema(responses=MailingStatSerializer(many=True))
    @action(detail=False, methods=["get"], url_path="stats")
    def all_stats(self, request):
        return self.list(request)

    @action(detail=True, methods=["get"], url_path="stats")
    def single_stats(self, request):
        return self.retrieve(request)
