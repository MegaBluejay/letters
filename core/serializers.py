from datetime import timedelta

import structlog
from django.utils import timezone
from django.utils.translation import gettext as _
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from rest_framework.relations import MANY_RELATION_KWARGS
from timezone_field.rest_framework import TimeZoneSerializerField

from .models import Client, Mailing, Tag
from .stats import filtered_fields
from .tasks import handle_mailing

logger: structlog.stdlib.BoundLogger = structlog.get_logger(__name__)


class TagManyField(serializers.ManyRelatedField):
    def to_internal_value(self, data):
        if isinstance(data, str) or not hasattr(data, "__iter__"):
            self.fail("not_a_list", input_type=type(data).__name__)
        if not self.allow_empty and len(data) == 0:
            self.fail("empty")

        try:
            return Tag.objects.bulk_create([Tag(name=name) for name in data], ignore_conflicts=True)
        except (TypeError, ValueError):
            raise serializers.ValidationError(_("Incorrect type"))


class TagField(serializers.PrimaryKeyRelatedField):
    queryset = Tag.objects.all()

    @classmethod
    def many_init(cls, **kwargs):
        list_kwargs = {"child_relation": cls(**kwargs)}
        for key in kwargs:
            if key in MANY_RELATION_KWARGS:
                list_kwargs[key] = kwargs[key]
        return TagManyField(**list_kwargs)

    def to_internal_value(self, data):
        return self.get_queryset().get_or_create(pk=data)[0]


class ClientSerializer(serializers.ModelSerializer):
    timezone = TimeZoneSerializerField()
    phone = serializers.RegexField(r"^7\d{10}$")
    tags = TagField(many=True)
    op_code = serializers.RegexField(r"\d{3}", read_only=True, required=False)

    class Meta:
        model = Client
        fields = ["id", "phone", "op_code", "tags", "timezone"]


class MailingSerializer(serializers.ModelSerializer):
    op_code = serializers.RegexField(r"\d{3}", required=False)
    tags = TagField(many=True)
    interval_start = serializers.TimeField(required=False)
    interval_end = serializers.TimeField(required=False)

    def validate(self, attrs):
        if attrs["end_dtm"] <= attrs["start_dtm"]:
            raise serializers.ValidationError("end_dtm <= start_dtm")

        if "interval_start" in attrs and "interval_end" in attrs and attrs["interval_end"] <= attrs["interval_start"]:
            raise serializers.ValidationError("interval_start <= interval_end")

        return attrs

    def save(self, **kwargs):
        mailing = super().save(**kwargs)
        now = timezone.now()
        if mailing.start_dtm <= now + timedelta(hours=1) and now < mailing.end_dtm:
            logger.info("immediate_handle", mailing_id=mailing.id)
            handle_mailing.apply_async([mailing.id], eta=mailing.start_dtm)
        return mailing

    class Meta:
        model = Mailing
        fields = ["id", "start_dtm", "end_dtm", "text", "op_code", "tags", "interval_start", "interval_end"]


class MailingStatSerializer(serializers.ModelSerializer):
    any_count = serializers.IntegerField()
    filtered_counts = serializers.SerializerMethodField()

    @extend_schema_field(
        {"type": "object", "properties": {filt: {"type": "integer"} for filt in filtered_fields.keys()}}
    )
    def get_filtered_counts(self, instance):
        return {filt: getattr(instance, field) for filt, field in filtered_fields.items()}

    class Meta:
        model = Mailing
        fields = ["id", "any_count", "filtered_counts"]
