from datetime import timedelta
from functools import wraps
from queue import Empty

import structlog
from celery.schedules import crontab
from celery.utils.log import get_task_logger
from django.conf import settings
from django.db import transaction
from django.db.models import Count, Q
from django.utils import timezone

from core import probe
from core.models import Client, Mailing, Message
from letters import celery_app

logger: structlog.stdlib.BoundLogger = structlog.wrap_logger(get_task_logger(__name__))

rate_limits = {
    "probe": settings.PROBE_LIMIT,
}


@celery_app.task
def token():
    return


# noinspection PyTypeChecker
@celery_app.on_after_finalize.connect
def startup(sender, **kwargs):
    for group, limit in rate_limits.items():
        sender.add_periodic_task(60 / limit, token.signature(queue=f"{group}_tokens"))
        sender.add_periodic_task(crontab(minute=0), check_mailings.s())
        sender.add_periodic_task(crontab(minute=30), rerun_failed.s())
    check_mailings.delay()


def rate_limit(group, bind=False):
    def decorator(func):
        @wraps(func)
        def wrapped(self, *args, **kwargs):
            with self.app.connection_for_read() as conn:
                with conn.SimpleQueue(f"{group}_tokens", no_ack=True, queue_opts={"max_length": 2}) as queue:
                    try:
                        queue.get(block=True, timeout=5)
                        if bind:
                            return func(self, *args, **kwargs)
                        else:
                            return func(*args, **kwargs)
                    except Empty:
                        self.retry(countdown=1)

        return wrapped

    return decorator


@celery_app.task(bind=True, max_retries=None)
@rate_limit("probe")
def probe_task(message_id):
    return probe.send_message(message_id)


@celery_app.task
def handle_mailing(mailing_id):
    log = logger.bind(mailing_id=mailing_id)
    with transaction.atomic():
        mailing = Mailing.objects.select_for_update().get(id=mailing_id)
        if mailing.handled:
            log.info("already_handled")
            return
        mailing.handled = True
        mailing.save()

        clients = Client.objects.all()

        tags = mailing.tags.values_list("name", flat=True)
        if tags:
            clients = clients.annotate(
                matching_tags=Count("tags", filter=Q(tags__name__in=tags), distinct=True)
            ).filter(matching_tags=len(tags))

        if mailing.op_code:
            clients = clients.filter(phone__regex=rf"^.{mailing.op_code}")

        client_ids = clients.values_list("id", flat=True)
        messages = [
            Message(status=Message.Status.PENDING, mailing=mailing, client_id=client_id) for client_id in client_ids
        ]
        Message.objects.bulk_create(messages)

        for message in messages:
            log.info("queuing_message", client_id=message.client_id, message_id=message.id)
            probe_task.delay(message.id)


@celery_app.task
def check_mailings():
    now = timezone.now()
    scheduled = Mailing.objects.filter(start_dtm__gte=now, start_dtm__lt=now + timedelta(hours=1))
    for mailing in scheduled:
        logger.info("handling_scheduled", mailing_id=mailing.id)
        handle_mailing.apply_async([mailing.id], eta=mailing.start_dtm)


@celery_app.task
def rerun_failed():
    now = timezone.now()
    failed = Message.objects.filter(status=Message.Status.FAILED).select_related("mailing")
    updated = []
    for message in failed:
        log = logger.bind(message_id=message.id, client_id=message.client_id, mailing_id=message.mailing_id)
        if message.mailing.start_dtm <= now < message.mailing.end_dtm:
            log.info("queuing_failed_message")
            probe_task.delay(message.id)
        else:
            log.info("out_of_range")
            message.status = Message.Status.CANCELLED
            updated.append(message)
    Message.objects.bulk_update(updated, fields=["status"])
