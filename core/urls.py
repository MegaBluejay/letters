from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView
from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, MailingViewSet

router = DefaultRouter()
router.register("clients", ClientViewSet, basename="client")
router.register("mailings", MailingViewSet, basename="mailing")

urlpatterns = [
    path("", include(router.urls)),
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
]
