from drf_spectacular.extensions import OpenApiSerializerFieldExtension


class TimeZoneSerializerFieldFix(OpenApiSerializerFieldExtension):
    target_class = "timezone_field.rest_framework.TimeZoneSerializerField"

    def map_serializer_field(self, auto_schema, direction):
        return {
            "type": "string",
            "format": "zoneinfo",
            "description": "String from zoneinfo time zone database",
            "example": "Europe/Moscow",
        }


class TimeSerializerFieldFix(OpenApiSerializerFieldExtension):
    target_class = "rest_framework.serializers.TimeField"

    def map_serializer_field(self, auto_schema, direction):
        return {
            "type": "string",
            "format": "time",
            "description": "ISO 8601 time",
            "example": "T042000",
        }
