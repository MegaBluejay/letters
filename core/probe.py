import httpx
import structlog
from django.conf import settings
from django.utils import timezone

from core.models import Message

logger: structlog.stdlib.BoundLogger = structlog.get_logger(__name__)


def send_message(message_id):
    log = logger.bind(message_id=message_id)
    try:
        message = Message.objects.select_related("client", "mailing").get(id=message_id)
    except Message.DoesNotExist:
        log.info("does_not_exist")
        return
    log = log.bind(client_id=message.client_id, mailing_id=message.mailing_id)

    if message.status not in [Message.Status.PENDING, Message.Status.FAILED]:
        log.info("not_pending_or_failed")
        return

    now = timezone.now()
    if not (message.mailing.start_dtm <= now < message.mailing.end_dtm):
        log.info("out_of_range")
        message.status = Message.Status.CANCELLED
        message.save()
        return
    if not check_interval(message, now):
        log.info("out_if_interval")
        message.status = Message.Status.FAILED
        message.failure = Message.Failure.INTERVAL
        message.save()
        return

    url = f"https://probe.fbrq.cloud/v1/send/{message.id}"
    data = {
        "id": message.id,
        "phone": message.client.phone,
        "text": message.mailing.text,
    }
    headers = {
        "Authorization": f"Bearer {settings.PROBE_TOKEN}",
    }
    try:
        response = httpx.post(url, data=data, headers=headers, timeout=settings.PROBE_TIMEOUT)
        response.raise_for_status()
    except httpx.HTTPError as e:
        log.info("api_error", exception=e)
        message.status = Message.Status.FAILED
        message.failure = Message.Failure.API_FAILURE
    else:
        log.info("sent")
        message.status = Message.Status.SENT
        message.failure = None
    message.save()


def check_interval(message, now):
    local_time = now.astimezone(message.client.timezone).time()
    start, end = message.mailing.interval_start, message.mailing.interval_end
    return (start is None or start <= local_time) and (end is None or local_time < end)
