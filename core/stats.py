from django.db.models import Count, Q

from core.models import Message

failure_fields = {failure: f"{failure}_count" for failure in Message.Failure.values}
status_fields = {status: f"{status}_count" for status in Message.Status.values}
filtered_fields = {**failure_fields, **status_fields}

failure_counts = {
    failure_field: Count("messages", filter=Q(messages__failure=failure), distinct=True)
    for failure, failure_field in failure_fields.items()
}
status_counts = {
    count_field: Count("messages", filter=Q(messages__status=status), distinct=True)
    for status, count_field in status_fields.items()
}

counts = {
    "any_count": Count("messages", distinct=True),
    **failure_counts,
    **status_counts,
}


def with_stats(queryset):
    return queryset.annotate(**counts)
