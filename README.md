# letters

## Дополнительные задания

- По адресу /docs доступно swagger ui
- Реализован docker-compose деплой
- Реализована обработка ошибок API
- Реализована проверка временного интервала
- Реализовано подробное грепабельное логирование

## Запуск проекта

### Локально

1. Установить [poetry](https://python-poetry.org)
2. Установить и запустить [postgres](https://www.postgresql.org), [redis](https://redis.io)
3. Скопировать `.env.template` в `.env`
4. Установить в нем значения в соответствии с окружением и `PROBE_TOKEN`, `SECRET_KEY`
5. Установить зависимости: `poetry install --with dev`.

Дальнейшие шаги выполнять в venv проекта (`poetry shell`)

6. Применить миграции: `./manage.py migrate`
7. Запустить в отдельном терминале celery beat: `celery -A letters beat -l INFO`
8. Запустить отдельном терминале celery worker: `celery -A letters worker -l INFO`
9. Запустить web-сервер: `./manage.py runserver`


### Docker

1. Установить и запустить [docker](https://www.docker.com)
2. Скопировать `.env.template` в `.env`
3. Установить в нем `PROBE_TOKEN`
4. Запустить проект: `docker compose up --build -d`
5. Применить миграции: `docker compose exec web ./manage.py migrate`

Для продакшн-запуска нужно также скопировать `.env.prod.template` в `.env.prod`
(значения приоритетнее тех, что в `.env`),
и использовать compose-файл `docker-compose.prod.yml`
